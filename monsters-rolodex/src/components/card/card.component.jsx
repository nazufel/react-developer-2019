import React from 'react';

import './card.styles.css';
// Card Component renders a card and takes in the monster prop and displays the name
export const Card = props => (
    <div className='card-container'>
    <img alt="monster" src={`https://robohash.org/${props.monster.id}?set=set2`} />
    <h2>{props.monster.name}</h2>
    <p>{props.monster.email}</p>
    </div>
)
