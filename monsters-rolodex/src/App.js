import React, {Component} from 'react';

//import {CardList} from './components/card-list/card-list.component'

import './App.css'
import { CardList } from './components/card-list/card-list.component';
import {SearchBox} from  './components/search-box/search-box.component';

// need to import Component to access it. 
// now created a class rather than original function to extend the app
// top level Component
class App extends Component {
  // constructor gives access to super
  constructor() {
    // super allows to set state
    super();
    // defining state objects
    // giving each of the state elements a unique ID to top console props errors and so React knows what element to update if changes occur. 
    this.state = {
      monsters: [],
      searchField: ''
    }
  }
  // life cycle methods

  // React will mount what it fetches
  componentDidMount() {
    // get stuff from the url
    // fetch returns a promise
    fetch('https://jsonplaceholder.typicode.com/users')
    // parse the response into json with a callback
    .then(response => response.json())
    // marshall json into state of monsters to the array of users with a callback
    .then(users => this.setState({monsters:users}))
  }

  // instantiate setting state for components
  handleChange =(e) => {
    // handle the change of the search field
    this.setState({searchField: e.target.value})
  };

  // rendering App by calling CardList Component and passing in monsters prop
  render() {
     const {monsters, searchField } = this.state; 
     const filteredMonsters = monsters.filter(monster => 
        monster.name.toLowerCase().includes(searchField.toLowerCase())
        )
    return (
      <div className="App">
      <h1>Monsters Rolodex</h1>
        <SearchBox
          placeholder='search monsters'
          handleChange={this.handleChange}
        />
        <CardList monsters={filteredMonsters}/>
      </div>
    );
  }
}


  // export the app
export default App;
